const Redis = require('ioredis');
const redis = new Redis();

let message1,
    message2,
    message3;

let channel1,
    channel2,
    channel3;


    message1 = {
        "_id" : "60f15afacc3ac173a9063884",
        "type": 5, 
        "merchant" : "606d5fc1f3caf56f28174452",
        "payment_service": "56e7ce796b6ef347d846e3eb",
        "card": {
            "number": "8600510389815555",
            "expire": "2508"
        },
        "amount": 50000,
        "payer": {
            "id": "5b91f0850d728f2663396fef",
            "phone": "991254544"
        }
    };

    message2 = {
        "_id" : "60f15afacc3ac173a9443884",
        "type": 2, 
        "merchant" : "606d5fc1f3caf56f28174452",
        "payment_service": "56e7ce796b6ef347d846e3eb",
        "card": {
            "number": "8600510854715555",
            "expire": "2411"
        },
        "amount": 75000,
        "payer": {
            "id": "5b91f0850d728fa45b396fef",
            "phone": "991277544"
        }
    };

    message3 = {
        "_id" : "60f15afacc3ac173a9783884",
        "type": 4, 
        "merchant" : "606d5fc1f3caf56f28174452",
        "payment_service": "56e7ce796b6ef347d846e3eb",
        "card": {
            "number": "8600510859658555",
            "expire": "2202"
        },
        "amount": 100000,
        "payer": {
            "id": "5b91f0850d7245cd5b396fef",
            "phone": "991285444"
        }
    };

    channel1 = 'p2p';
    channel2 = 'mesta';
    channel3 = 'onlineStore';



    let func1 = () => {
        redis.publish(channel1, JSON.stringify(message1));
    };

    let func2 = () => {
        redis.publish(channel2, JSON.stringify(message2));
    };

    let func3 = () => {
        redis.publish(channel3, JSON.stringify(message3));
    };




    let functions = [func1, func2, func3];

    function randomNumber(n) { 
        return Math.floor( Math.random() * n ); 
    } 




setInterval(() => {

    try {
        
        functions[randomNumber(functions.length)](); 

    } catch (err) {

        console.error(err);

    }

}, 1000);